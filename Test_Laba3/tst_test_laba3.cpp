﻿#include <QtTest>
#include <../functions.h>

// add necessary includes here

class Test_Laba3 : public QObject
{
    Q_OBJECT

public:
    Test_Laba3();
    ~Test_Laba3();

private slots:
    void test_binToDec();
    void test_decToBin();
    void test_binToOct();
    void test_octToBin();
    void test_octToHex();
    void test_hexToOct();

};

Test_Laba3::Test_Laba3()
{

}

void Test_Laba3::test_binToDec()
{
    QString num("10010");
    QCOMPARE("18", binToDec(num));
}

void Test_Laba3::test_decToBin()
{
    QString num("20");
    QCOMPARE("10100", decToBin(num));
}

void Test_Laba3::test_binToOct()
{
    QString num("11101");
    QCOMPARE("35", binToOct(num));
}

void Test_Laba3::test_octToBin()
{
    QString num("42");
    QCOMPARE("100010",octToBin(num));
}

void Test_Laba3::test_octToHex()
{
    QString num("74");
    QCOMPARE("3c",octToHex(num));
}

void Test_Laba3::test_hexToOct()
{
    QString num("e9");
    QCOMPARE("351",hexToOct(num));
}
