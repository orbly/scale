﻿/**
*\file
\brief Программа для перевода числа в различные системы счисления
\author orbly
\version 1.0
*/

#include <QCoreApplication>
#include "functions.h"
#include <QTextCodec>
#include <iostream>
#include <QString>
#include <stdlib.h>
#include <QRegExp>
#include <conio.h>
#include <QTextStream>

using namespace std;

/*!
\brief Функция, определяющая пункт меню
\param[out] choice Пункт меню
*\code
int menu()
{
    int choice;
    cin >> choice;
    return choice;
}
*\endcode
*/

int menu()
{
    int choice;
    cin >> choice;
    return choice;
}

/*!
\brief Точка входа в программу
*/

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

#ifdef Q_OS_WIN32
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
#endif

    char checking = '\0';
    QString number;
    QTextStream qcin(stdin);
    QTextStream qcout(stdout);

    QRegExp rxBin("^[01]+$");
    QRegExp rxOct("^[0-7]+$");
    QRegExp rxDec("^[0-9]+$");
    QRegExp rxHex("^[0123456789abcdef]+$");
    bool exit = true;
    bool printedMenu = true;

    while (exit)
    {
        if (printedMenu)
        {
            system("cls");
            cout << QString::fromUtf8("Выберите основание системы счисления:\n1. 2\n2. 8\n3. 10\n4. 16\nДля ввыхода из программы выберите 5.").toLocal8Bit().data() << endl;
            printedMenu = false;
        }

        int checker;
        QString result;

        switch (menu())
        {
            case 1:
            system("cls");

            cout << QString::fromUtf8("Введите число:").toLocal8Bit().data() << endl;
            qcin >> number >> endl;
            while (rxBin.exactMatch(number) == false)
            {
                cout << QString::fromUtf8("Число введено неверно. Введите двоичное число").toLocal8Bit().data() << endl;
                qcin >> number >> endl;
            }

            cout << QString::fromUtf8("Выберите, в какую систему вы хотите перевести число:\n1. В десятичную\n2. В восьмеричную").toLocal8Bit().data() << endl;
            while (scanf("%d", &checker) != 1 || checker < 1 || checker > 2)
            {
                while (scanf("%c", &checking) != 0 && checking != '\n');
                cout << QString::fromUtf8("Вы ввели неверное значение, попробуйте еще раз").toLocal8Bit().data() << endl;
            }

            switch (checker)
            {
                case 1:
                result = binToDec(number);
                cout << QString::fromUtf8("Число в десятичной системе:").toLocal8Bit().data() << endl;
                qcout << result << endl;
                break;

                case 2:
                result = binToOct(number);
                cout << QString::fromUtf8("Число в восьмеричной системе:").toLocal8Bit().data() << endl;
                qcout << result << endl;
                break;
            }
            printedMenu = true;
            getchar();
			getchar();
            break;

            case 2:
            system("cls");
            cout << QString::fromUtf8("Введите число:").toLocal8Bit().data() << endl;
            qcin >> number >> endl;
            while (rxOct.exactMatch(number) == false)
            {
                cout << QString::fromUtf8("Число введено неверно. Введите восьмеричное число").toLocal8Bit().data() << endl;
                qcin >> number >> endl;
            }

            cout << QString::fromUtf8("Выберите, в какую систему вы хотите перевести число:\n1. В двочиную\n2. В шестнадцетиричную").toLocal8Bit().data() << endl;
            while (scanf("%d", &checker) != 1 || checker < 1 || checker > 2)
            {
                while (scanf("%c", &checking) != 0 && checking != '\n');
                cout << QString::fromUtf8("Вы ввели неверное значение, попробуйте еще раз").toLocal8Bit().data() << endl;
            }

            switch (checker)
            {
                case 1:
                result = octToBin(number);
                cout << QString::fromUtf8("Число в двоичной системе:").toLocal8Bit().data() << endl;
                qcout << result << endl;
                break;

                case 2:
                result = octToHex(number);
                cout << QString::fromUtf8("Число в шестнадцетиричной системе:").toLocal8Bit().data() << endl;
                qcout << result << endl;
                break;
            }
            printedMenu = true;
            getchar();
			getchar();
            break;

            case 3:
            system("cls");
            cout << QString::fromUtf8("Введите число:").toLocal8Bit().data() << endl;
            qcin >> number >> endl;
            while (rxDec.exactMatch(number) == false)
            {
                cout << QString::fromUtf8("Число введено неверно.").toLocal8Bit().data() << endl;
                qcin >> number >> endl;
            }
            result = decToBin(number);
            cout << QString::fromUtf8("Число в двоичной системе:").toLocal8Bit().data() << endl;
            qcout << result << endl;
            printedMenu = true;
            getchar();
			getchar();
            break;

            case 4:
            system("cls");
            cout << QString::fromUtf8("Введите число:").toLocal8Bit().data() << endl;
            qcin >> number >> endl;
            while (rxHex.exactMatch(number) == false)
            {
                cout << QString::fromUtf8("Число введено неверно. Введите шестнадцетиричное число").toLocal8Bit().data() << endl;
                qcin >> number >> endl;
            }
            result = hexToOct(number);
            cout << QString::fromUtf8("Число в восьмеричной системе:").toLocal8Bit().data() << endl;
            qcout << result << endl;
            printedMenu = true;
            getchar();
			getchar();
            break;

            case 5:
            exit = false;
            break;

            default:
            cout << QString::fromUtf8("Вы ввели неверное значение, попробуйте еще раз").toLocal8Bit().data() << endl;
            break;


        }
    }
    cout << QString::fromUtf8("Нажмите любую клавишу").toLocal8Bit().data() << endl;
    return 0;

    return a.exec();
}
