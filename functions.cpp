﻿/**
*\file
\brief Функции перевода чисел в различные СС
*/

#include "functions.h"
#include <QString>

/*!
\brief Функция перевода числа из двоичной СС в десятичную
\param[in] num Исходное число
\param[out] decStr Переведенное число
*\code
QString binToDec (QString num)
{
    QString decStr;
    bool ok;
    int dec = num.toInt(&ok,2);

    if (ok)
    {
        decStr.setNum(dec);
        return decStr;
    }
    else return nullptr;
}
*\endcode
*/

QString binToDec (QString num)
{
    QString decStr;
    bool ok;
    int dec = num.toInt(&ok,2);

    if (ok)
    {
        decStr.setNum(dec);
        return decStr;
    }
    else return nullptr;
}

/*!
\brief Функция перевода числа из десятичной СС в двоичную
\param[in] num Исходное число
\param[out] binStr Переведенное число
*\code
QString decToBin (QString num)
{
    QString binStr;
    int dec = num.toInt();
    binStr = QString("%1").arg(dec,0,2);
    return binStr;
}
*\endcode
*/

QString decToBin (QString num)
{
    QString binStr;
    int dec = num.toInt();
    binStr = QString("%1").arg(dec,0,2);
    return binStr;
}

/*!
\brief Функция перевода числа из двоичной СС в восьмеричную
\param[in] num Исходное число
\param[out] octStr Переведенное число
*\code
QString binToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 2);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}
*\endcode
*/


QString binToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 2);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}

/*!
\brief Функция перевода числа из восьмеричной СС в двоичную
\param[in] num Исходное число
\param[out] binStr Переведенное число
*\code
QString octToBin (QString num)
{
    QString binStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        binStr = QString("%1").arg(dec,0,2);
        return binStr;
    }
    else return nullptr;
}
*\endcode
*/

QString octToBin (QString num)
{
    QString binStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        binStr = QString("%1").arg(dec,0,2);
        return binStr;
    }
    else return nullptr;
}

/*!
\brief Функция перевода числа из восьмеричной СС в шестнадцетиричную
\param[in] num Исходное число
\param[out] hexStr Переведенное число
*\code
QString octToHex (QString num)
{
    QString hexStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        hexStr = QString("%1").arg(dec,0,16);
        return hexStr;
    }
    else return nullptr;
}
*\endcode
*/

QString octToHex (QString num)
{
    QString hexStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        hexStr = QString("%1").arg(dec,0,16);
        return hexStr;
    }
    else return nullptr;
}

/*!
\brief Функция перевода числа из шестнадцетиричной СС в восьмеричную
\param[in] num Исходное число
\param[out] octStr Переведенное число
*\code
QString hexToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 16);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}
*\endcode
*/

QString hexToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 16);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}
